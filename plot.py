import pandas as pd
import numpy as np
import tensorflow as tf
import datetime as dt
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.use("Agg")

import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

import glob

import src.models as models
from src.dataset import Dataset
from tensorflow.keras.models import load_model
from src.customs import (
    custom_loss,
    custom_mape,
    custom_acc,
    custom_acc_0,
    custom_acc_loss,
    false_negative,
    false_positive,
    accuracy_entropy,
    cat_false_negative,
    cat_false_positive,
    cat_accuracy_entropy,
    cat_accuracy_entropy_score,
)

dtNow = dt.datetime.utcnow()

input_path = '/storagefinep/wrf/dataset/{date:%Y%m%d}_wrf_dataset.nc'
output_path = '/storagefinep/merge/dataset/{date:%Y%m%d}_merge_dataset.nc'

date_ini = '2021-01-01'
date_fin = '2021-07-27'

dates = pd.date_range(date_ini, date_fin, freq='D').to_pydatetime()
suffix = 'poc06'
# fmodel = './models/dafuq.h5'
fmodel = sorted(glob.glob(f'./models/*_aira2_{suffix}.h5'))[-1]

# fmodel = './models/best_20210701.h5'
# fmodel = './milestone_models/aira2_cat4_v1.h5'

print(f'using: {fmodel}')

def categorize_values(values, cats):
    _temp_values = np.zeros_like(values, dtype=int)
    _rules = _rules = [(True if left is None else values >= left) & (True if right is None else values < right) for left, right in cats]
    for idx, _rule in enumerate(_rules):
            _temp_values[_rule] = idx
    return _temp_values


'''
model = load_model(fmodel, custom_objects={'custom_loss': custom_loss,
                                           'custom_mape': custom_mape,
                                           'custom_acc': custom_acc,
                                           'custom_acc_0': custom_acc_0,
                                           'custom_acc_loss': custom_acc_loss,
                                           'false_positive': false_positive,
                                           'false_negative': false_negative,
                                           'accuracy_entropy': accuracy_entropy,
                                           'cat_false_positive': cat_false_positive,
                                           'cat_false_negative': cat_false_negative,
                                           'cat_accuracy_entropy': cat_accuracy_entropy,
                                           'cat_accuracy_entropy_score': cat_accuracy_entropy_score,
                                          })
'''
                                          
model = models.model_3d((3, None, None, 145), custom_loss)
model.load_weights(fmodel)
print(model.summary())

_norms = {'hgtprs': 5800., 
          'tmpprs': 204., 
          'rhprs': 100., 
          'dptprs': 298., 
          'wind_speed': 30., 
          'wind_dir': 360., 
          'tmpsfc': 305., 
          'tmp2m': 310., 
          'rh2m': 100., 
          'wind_direction': 360., 
          'wind_magnitude': 30., 
          'acpcpsfc': 150., 
          'cpratsfc': 150.,
          'apcpsfc': 100.,
          'crainsfc': 100.,
          'pratesfc': 100., 
          'capesfc': 50.,
          }
          
_l_norms = {'precipitation': 100.,
            }

cat9 = [[None, 0.005],
        [0.005, 0.01],
        [0.01,  0.03],
        [0.03,  0.05],
        [0.05,  0.10],
        [0.10,  0.15],
        [0.15,  0.25],
        [0.25,  0.30],
        [0.30,  None]]
        
cat4 = [[None, 0.01],
        [0.01, 0.15],
        [0.15, 0.30],
        [0.30, None]]
        
cat9_mids = np.asanyarray([0.0025, 0.0075, 0.02, 0.04, 0.075, 0.125, 0.2, 0.275, 0.65])
cat4_mids = np.asanyarray([0.005, 0.08, 0.225, 0.65])
        
dot9 = [0, 1, 3, 5, 10, 15, 25, 30, 100]
dot4 = [0, 8, 23, 65]

rules = {4: {'cat': cat4, 'dot': dot4, 'mids': cat4_mids},
         9: {'cat': cat9, 'dot': dot9, 'mids': cat9_mids}}

rule = 9
use_test = True
datasets = Dataset(input_path,
                   output_path,
                   dates,
                   # stacking=False,
                   timesteps=3,
                   norms=_norms,
                   l_norms=_l_norms,
                   cat=rules[rule]['cat'],
                   test_slice_head=use_test)

proj = ccrs.epsg(3857)

# total_power = []
def prepare_ax(datasets, *args, proj=proj):
    ax = plt.subplot(*args, projection=proj)
    ax.set_extent((datasets.general_lons.min(),
                   datasets.general_lons.max(),
                   datasets.general_lats.min(),
                   datasets.general_lats.max()),
                  crs=ccrs.PlateCarree())
    return ax
    
    
def prepare_coastline(ax):
    ax.coastlines(color='white', alpha=0.8)
    ax.gridlines(crs=ccrs.PlateCarree(), linestyle='--',
                 color='gray', draw_labels=True, alpha=0.8)
                     
# test data
_in = datasets._x_test
_out = datasets._y_test_numerical
_out_cat = datasets._y_test
    
y_test = _out_cat
    
print(model.evaluate(_in, y_test)) # cat + num
    
yhat = model.predict(_in) # [-1]
if isinstance(yhat, list):
    yhat = yhat[-1]
    
vmin, vmax = 0., _out.max() * _l_norms['precipitation']

_size = datasets.idx_y_test.size // 5

frc_maps = []
obs_maps = []

for idx, (y, l, i, time_idx, l_cat) in enumerate(zip(yhat, _out, _in, datasets.idx_y_test, _out_cat)):

    if not use_test:
        date = datasets.dates[-5:][idx // _size]
    else:
        date = datasets.dates[idx // _size]
    print(date)
    
    ###
    # frc_maps.append(y.dot(rules[rule]['dot']))
    frc_maps.append(rules[rule]['mids'][y.dot(np.arange(rule) + 0.5).astype(int)] * _l_norms['precipitation'])
    obs_maps.append(l[..., 0] * _l_norms['precipitation'])
    ###
    # continue
    ### PLOT CATEGORIES
    num_categories = y.shape[-1]
    fig = plt.figure(figsize=(7. * num_categories, 7.))
    
    for cat in range(num_categories):
    
        plt.subplot(1, num_categories, cat + 1)
        plt.pcolormesh(datasets.general_lons, datasets.general_lats, y[..., cat], vmin=0., vmax=1., cmap='cividis')
        plt.xlabel('lon')
        plt.ylabel('lat')
        plt.title('frc cat %d'%cat)
        plt.colorbar()
    fig.suptitle('Forecast at: %sT%02d:00'%(date.strftime("%Y-%m-%d"), time_idx) + '\n' + str(rules[rule]['dot']))
    
    plt.savefig(f"./imgs/cat_%s_%s_%d_{suffix}.png"%(fmodel[:-3].split('/')[-1], date.strftime("%Y%m%d"), idx))
    plt.close()
    ###
    
    
    
    ### PLOT FRC + DIFF + OBS
    fig = plt.figure(figsize=(21., 7.))
    
    # plt.subplot(1, 3, 1)
    ax = prepare_ax(datasets, 1, 3, 1)
    plt.pcolormesh(datasets.general_lons,
                   datasets.general_lats,
                   # y.dot(rules[rule]['dot']),
                   rules[rule]['mids'][y.dot(np.arange(rule) + 0.5).astype(int)] * _l_norms['precipitation'],
                   # y[..., 0]  * _l_norms['precipitation'],
                   vmin=0.,
                   vmax=min(vmax, _l_norms['precipitation']),
                   cmap='cividis',
                   transform=ccrs.PlateCarree())
    prepare_coastline(ax)
    plt.xlabel('lon')
    plt.ylabel('lat')
    plt.title('frc mids')
    plt.colorbar()
    
    # diff = y.dot(rules[rule]['dot']) - l[..., 0] * _l_norms['precipitation']
    diff = (rules[rule]['mids'][y.dot(np.arange(rule) + 0.5).astype(int)] - l[..., 0]) * _l_norms['precipitation']
    # plt.subplot(1, 3, 2)
    ax = prepare_ax(datasets, 1, 3, 2)
    plt.pcolormesh(datasets.general_lons,
                   datasets.general_lats,
                   diff,
                   vmin=-abs(diff).max(),
                   vmax=abs(diff).max(),
                   cmap='bwr',
                   transform=ccrs.PlateCarree())
    prepare_coastline(ax)
    plt.xlabel('lon')
    plt.ylabel('lat')
    plt.title('diff')
    plt.colorbar()
    
    # plt.subplot(1, 3, 3)
    ax = prepare_ax(datasets, 1, 3, 3)
    plt.pcolormesh(datasets.general_lons,
                   datasets.general_lats,
                   l[..., 0] * _l_norms['precipitation'],
                   vmin=0.,
                   vmax=min(vmax, _l_norms['precipitation']),
                   cmap='cividis',
                   transform=ccrs.PlateCarree())
    prepare_coastline(ax)
    plt.xlabel('lon')
    plt.ylabel('lat')
    plt.title('obs')
    plt.colorbar()
    
    fig.suptitle('Forecast at: %sT%02d:00'%(date.strftime("%Y-%m-%d"), time_idx))
    
    plt.savefig(f'./imgs/frc_diff_obs_%s_%s_%d_{suffix}.png'%(fmodel[:-3].split('/')[-1], date.strftime("%Y%m%d"), idx))
    plt.close()
    ###
    
    
    
    '''
    ### PLOT FRC + WRF + OBS
    fig = plt.figure(figsize=(21., 7.))
    
    # plt.subplot(1, 3, 1)
    ax = prepare_ax(datasets, 1, 3, 1)
    plt.pcolormesh(datasets.general_lons,
                   datasets.general_lats,
                   y.dot(rules[rule]['dot']),
                   # y[..., 0]  * _l_norms['precipitation'],
                   vmin=0.,
                   vmax=min(vmax, _l_norms['precipitation']),
                   cmap='cividis',
                   transform=ccrs.PlateCarree())
    prepare_coastline(ax)
    plt.xlabel('lon')
    plt.ylabel('lat')
    plt.title('frc dot')
    plt.colorbar()
    
    # plt.subplot(1, 3, 2)
    ax = prepare_ax(datasets, 1, 3, 2)
    plt.pcolormesh(datasets.general_lons,
                   datasets.general_lats,
                   (datasets._x[time_idx, ..., datasets._var_header[ 'apcpsfc']] - datasets._x[time_idx - 1, ..., datasets._var_header[ 'apcpsfc']]) * datasets._per_variable_norm[ 'apcpsfc'] +\
                   (datasets._x[time_idx, ..., datasets._var_header['acpcpsfc']] - datasets._x[time_idx - 1, ..., datasets._var_header['acpcpsfc']]) * datasets._per_variable_norm['acpcpsfc'],
                   vmin=0.,
                   vmax=min(vmax, _l_norms['precipitation']),
                   cmap='cividis',
                   transform=ccrs.PlateCarree())
    prepare_coastline(ax)
    plt.xlabel('lon')
    plt.ylabel('lat')
    plt.title('wrf apcpsfc + acpcpsfc')
    plt.colorbar()
    
    # plt.subplot(1, 3, 3)
    ax = prepare_ax(datasets, 1, 3, 3)
    plt.pcolormesh(datasets.general_lons,
                   datasets.general_lats,
                   l[..., 0] * _l_norms['precipitation'],
                   vmin=0.,
                   vmax=min(vmax, _l_norms['precipitation']),
                   cmap='cividis',
                   transform=ccrs.PlateCarree())
    prepare_coastline(ax)
    plt.xlabel('lon')
    plt.ylabel('lat')
    plt.title('obs')
    plt.colorbar()
    
    fig.suptitle('Forecast at: %sT%02d:00'%(date.strftime("%Y-%m-%d"), time_idx))
    
    plt.savefig(f'./imgs/frc_wrf_obs_%s_%s_%d_{suffix}.png'%(fmodel[:-3].split('/')[-1], date.strftime("%Y%m%d"), idx))
    plt.close()
    ###
    '''
    
    ### PLOT FRC + OBS
    fig = plt.figure(figsize=(14., 7.))
    
    
    '''
    ax = plt.subplot(1, 2, 1, projection=proj)
    ax.set_extent((datasets.general_lons.min(),
                   datasets.general_lons.max(),
                   datasets.general_lats.min(),
                   datasets.general_lats.max()),
                  crs=ccrs.PlateCarree())
    '''
    ax = prepare_ax(datasets, 1, 2, 1)
    plt.pcolormesh(datasets.general_lons,
                   datasets.general_lats,
                   # y.dot(rules[rule]['dot']),
                   rules[rule]['mids'][y.dot(np.arange(rule) + 0.5).astype(int)] * _l_norms['precipitation'],
                   # y[..., 0] * _l_norms['precipitation'],
                   vmin=0.,
                   vmax=vmax,
                   cmap='cividis',
                   transform=ccrs.PlateCarree())
    '''
    ax.coastlines(color='white', alpha=0.8)
    ax.gridlines(crs=ccrs.PlateCarree(), linestyle='--',
                 color='gray', draw_labels=True, alpha=0.8)
    '''
    prepare_coastline(ax)
    plt.xlabel('lon')
    plt.ylabel('lat')
    plt.title('frc mids')
    plt.colorbar()
    
    # plt.subplot(1, 2, 2)
    ax = prepare_ax(datasets, 1, 2, 2)
    plt.pcolormesh(datasets.general_lons,
                   datasets.general_lats,
                   l[..., 0] * _l_norms['precipitation'],
                   vmin=0.,
                   vmax=vmax,
                   cmap='cividis',
                   transform=ccrs.PlateCarree())
    prepare_coastline(ax)
    plt.xlabel('lon')
    plt.ylabel('lat')
    plt.title('obs')
    plt.colorbar()
    
    mse = np.mean(rules[rule]['mids'][y.dot(np.arange(rule) + 0.5).astype(int)] * _l_norms['precipitation'] - l[..., 0] * _l_norms['precipitation'])**2
    fig.suptitle('Forecast at: %sT%02d:00'%(date.strftime("%Y-%m-%d"), time_idx) + ' | mse: %.3f'%mse)
    # fig.suptitle('Forecast at: %sT%02d:00'%(date.strftime("%Y-%m-%d"), time_idx) + ' | mse: %.3f'%np.mean((y[..., 0] * _l_norms['precipitation'] - l[..., 0] * _l_norms['precipitation'])**2))
    plt.tight_layout()
    
    plt.savefig(f'./imgs/frc_obs_%s_%s_%d_{suffix}.png'%(fmodel[:-3].split('/')[-1], date.strftime("%Y%m%d"), idx))
    plt.close()
    ###
    '''
    ### CHECK POWER
    
    flat_label = l_cat.flatten().astype(int)
    flat_yhat = y.reshape(-1, num_categories)
    w = np.eye(rule)
    
    powers = np.arange(0.5, 3.1, 0.05)
    res = []
    for power in powers:
        # print('power test:', power)
        contingency = np.zeros((num_categories, num_categories))
        occurr = []
        for cat in range(num_categories):
            cat_true = flat_label == cat
            reindexing_values = np.power(flat_yhat[cat_true], power).dot(rules[rule]['dot'])
            reindexing = categorize_values(reindexing_values, rules[rule]['cat'])
            contingency[cat, :] = w[reindexing].sum(axis=0)
            contingency[cat, :] /= contingency[cat, :].sum() or 1.
            occurr.append(cat_true.sum())
            
        res.append(np.diag(contingency).sum() - np.diag(contingency, k=2).sum() - np.diag(contingency, k=3).sum() - np.diag(contingency, k=-2).sum() - np.diag(contingency, k=-3).sum())
        
    print('ideal power:', powers[np.argmax(res)])
    total_power.append(powers[np.argmax(res)])
    # plt.plot(powers, res)
    # plt.savefig('./imgs/testpower%d.png'%idx)
    # plt.close()
    ###
    '''
    
    ### PLOT CONTINGENCY
    
    contingency = np.zeros((num_categories, num_categories))
    flat_label = l_cat.reshape(-1, num_categories)
    flat_yhat = y.reshape(-1, num_categories)
    where_argmax = np.argmax(flat_yhat, axis=1)
    # one_hot_yhat = np.zeros_like(flat_yhat)
    # one_hot_yhat[(tuple(np.arange(where_argmax.size)), tuple(where_argmax))] = 1.
    
    w = np.eye(rule)
    
    # print(flat_yhat.shape, one_hot_yhat.shape)
    
    occurr = []
    for cat in range(num_categories):
        cat_true = np.argmax(flat_label, axis=1) == cat
        reindexing = np.argmax(flat_yhat[cat_true], axis=1)
        # reindexing_values = flat_yhat[cat_true].dot(rules[rule]['dot'])
        # reindexing = categorize_values(reindexing_values, rules[rule]['cat'])
        contingency[cat, :] = w[reindexing].sum(axis=0)
        contingency[cat, :] /= contingency[cat, :].sum() or 1.
        occurr.append(cat_true.sum())
        # contingency[cat, :] = one_hot_yhat[cat_true].sum(axis=0) / one_hot_yhat[cat_true].sum()
        
    fig, ax = plt.subplots(figsize=(7., 7.))
        
    ax.imshow(contingency, vmin=0., vmax=max(1., contingency.max()), cmap='cividis')
    ax.set_xticks(np.arange(num_categories))
    ax.set_yticks(np.arange(num_categories))
    ax.set_yticklabels(occurr)
    ax.set_xlabel('frc')
    ax.set_ylabel('ocorrencias')
    
    # ax2 = ax.twinx()
    # ax2.imshow()
    # ax2.set_yticks(np.arange(num_categories))
    # ax2.set_yticklabels(occurr)
    # ax2.set_ylabel('total occurrence')
    
    thres = contingency.ptp() / 2.
    
    for i in range(num_categories):
        for j in range(num_categories):
            ax.text(j, i, '%.3f'%contingency[i, j], ha='center', va='center', color='k' if contingency[i, j] > thres else 'w')
    
    ax.set_title('Forecast at: %sT%02d:00'%(date.strftime("%Y-%m-%d"), time_idx))
    fig.tight_layout()
    
    plt.savefig(f'./imgs/contingency_%s_%s_%d_{suffix}.png'%(fmodel[:-3].split('/')[-1], date.strftime("%Y%m%d"), idx))
    plt.close()
    ###
    
    
    
    ### PLOT CONTINGENCY MEAN
    
    contingency = np.zeros((num_categories, num_categories))
    flat_label = l_cat.reshape(-1, num_categories)
    flat_yhat = y.reshape(-1, num_categories)
    where_argmax = flat_yhat.dot(np.arange(num_categories)).astype(int)
    # one_hot_yhat = np.zeros_like(flat_yhat)
    # one_hot_yhat[(tuple(np.arange(where_argmax.size)), tuple(where_argmax))] = 1.
    
    w = np.eye(rule)
    
    # print(flat_yhat.shape, one_hot_yhat.shape)
    
    occurr = []
    for cat in range(num_categories):
        cat_true = np.argmax(flat_label, axis=1) == cat
        reindexing = flat_yhat[cat_true].dot(np.arange(num_categories) + 0.5).astype(int)
        # reindexing_values = flat_yhat[cat_true].dot(rules[rule]['dot'])
        # reindexing = categorize_values(reindexing_values, rules[rule]['cat'])
        contingency[cat, :] = w[reindexing].sum(axis=0)
        contingency[cat, :] /= contingency[cat, :].sum() or 1.
        occurr.append(cat_true.sum())
        # contingency[cat, :] = one_hot_yhat[cat_true].sum(axis=0) / one_hot_yhat[cat_true].sum()
        
    fig, ax = plt.subplots(figsize=(7., 7.))
        
    ax.imshow(contingency, vmin=0., vmax=max(1., contingency.max()), cmap='cividis')
    ax.set_xticks(np.arange(num_categories))
    ax.set_yticks(np.arange(num_categories))
    ax.set_yticklabels(occurr)
    ax.set_xlabel('frc')
    ax.set_ylabel('ocorrencias')
    
    # ax2 = ax.twinx()
    # ax2.imshow()
    # ax2.set_yticks(np.arange(num_categories))
    # ax2.set_yticklabels(occurr)
    # ax2.set_ylabel('total occurrence')
    
    thres = contingency.ptp() / 2.
    
    for i in range(num_categories):
        for j in range(num_categories):
            ax.text(j, i, '%.3f'%contingency[i, j], ha='center', va='center', color='k' if contingency[i, j] > thres else 'w')
    
    ax.set_title('Forecast at: %sT%02d:00'%(date.strftime("%Y-%m-%d"), time_idx))
    fig.tight_layout()
    
    plt.savefig(f'./imgs/contingency_mean_%s_%s_%d_{suffix}.png'%(fmodel[:-3].split('/')[-1], date.strftime("%Y%m%d"), idx))
    plt.close()
    ###
    
    
    
frc_maps = np.asanyarray(frc_maps)
obs_maps = np.asanyarray(obs_maps)

mae_map = abs(frc_maps - obs_maps).mean(axis=0)
mse_map = np.power(frc_maps - obs_maps, 2).mean(axis=0)
mse_99_map = mse_map.copy()
mse_99_map[mse_99_map >= np.percentile(mse_99_map, 99)] = np.nan
p0_map = ((frc_maps  < 1.) & (obs_maps  < 1.)).sum(axis=0) / (obs_maps  < 1.).sum(axis=0)
p1_map = ((frc_maps >= 1.) & (obs_maps >= 1.)).sum(axis=0) / (obs_maps >= 1.).sum(axis=0)
p5_map = ((frc_maps >= 5.) & (obs_maps >= 5.)).sum(axis=0) / (obs_maps >= 5.).sum(axis=0)

### PLOT FINAL STATS
fig = plt.figure(figsize=(7. * 6, 7.))

ax = prepare_ax(datasets, 1, 6, 1)
plt.pcolormesh(datasets.general_lons,
               datasets.general_lats,
               mae_map,
               vmin=0.,
               # vmax=vmax,
               cmap='cividis',
               transform=ccrs.PlateCarree())
prepare_coastline(ax)
plt.xlabel('lon')
plt.ylabel('lat')
plt.title('mae')
plt.colorbar()

ax = prepare_ax(datasets, 1, 6, 2)
plt.pcolormesh(datasets.general_lons,
               datasets.general_lats,
               mse_map,
               vmin=0.,
               # vmax=vmax,
               cmap='cividis',
               transform=ccrs.PlateCarree())
prepare_coastline(ax)
plt.xlabel('lon')
plt.ylabel('lat')
plt.title('mse')
plt.colorbar()

ax = prepare_ax(datasets, 1, 6, 3)
plt.pcolormesh(datasets.general_lons,
               datasets.general_lats,
               mse_99_map,
               vmin=0.,
               # vmax=vmax,
               cmap='cividis',
               transform=ccrs.PlateCarree())
prepare_coastline(ax)
plt.xlabel('lon')
plt.ylabel('lat')
plt.title('mse | percentil 99')
plt.colorbar()

ax = prepare_ax(datasets, 1, 6, 4)
plt.pcolormesh(datasets.general_lons,
               datasets.general_lats,
               p0_map,
               vmin=0.,
               vmax=1.,
               cmap='cividis',
               transform=ccrs.PlateCarree())
prepare_coastline(ax)
plt.xlabel('lon')
plt.ylabel('lat')
plt.title('acc prec < 1')
plt.colorbar()

ax = prepare_ax(datasets, 1, 6, 5)
plt.pcolormesh(datasets.general_lons,
               datasets.general_lats,
               p1_map,
               vmin=0.,
               vmax=1.,
               cmap='cividis',
               transform=ccrs.PlateCarree())
prepare_coastline(ax)
plt.xlabel('lon')
plt.ylabel('lat')
plt.title('acc prec >= 1')
plt.colorbar()

ax = prepare_ax(datasets, 1, 6, 6)
plt.pcolormesh(datasets.general_lons,
               datasets.general_lats,
               p5_map,
               vmin=0.,
               vmax=1.,
               cmap='cividis',
               transform=ccrs.PlateCarree())
prepare_coastline(ax)
plt.xlabel('lon')
plt.ylabel('lat')
plt.title('acc prec >= 5')
plt.colorbar()
    
fig.suptitle('stats')

plt.savefig(f"./imgs/stats_%s_{suffix}.png"%(fmodel[:-3].split('/')[-1]))
plt.close()
###