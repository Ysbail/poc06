import numpy as np
import tensorflow as tf
import tensorflow.keras.backend as K
from keras.losses import (
    BinaryCrossentropy,
    SparseCategoricalCrossentropy,
    categorical_crossentropy,
)

def custom_loss(y_true, y_pred):
    # loss = 1. - K.mean(K.exp(-K.pow(y_true - y_pred, 2)))
    
    # loss_mape = K.mean(K.abs(y_true - y_pred) / K.clip(y_true, K.epsilon(), None))
    # loss_mae = K.mean(K.abs(y_true - y_pred))
    # loss_mse = K.mean(K.pow(y_true - y_pred, 2))
    # fn = false_negative(y_true, y_pred)
    # fp = false_positive(y_true, y_pred)
    # loss_scc = SparseCategoricalCrossentropy()(y_true, y_pred)
    # loss_bc = BinaryCrossentropy()(y_true, y_pred)
    fnr = cat_false_negative(y_true, y_pred)
    fpr = cat_false_positive(y_true, y_pred)
    
    return categorical_crossentropy(y_true, y_pred) + fnr + fpr 


_epsilon = K.constant(K.epsilon(), 'float32')
def _custom_categorical_crossentropy(y_true, y_pred):
    y_true = K.cast(y_true, 'float32')
    y_pred = K.cast(y_pred, 'float32')

    w_sum = K.sum(y_true, axis=list(range(1, y_true.shape.ndims - 1)))
    w_sum = tf.clip_by_value(w_sum / K.sum(w_sum, axis=-1, keepdims=True), _epsilon, 1. - _epsilon)
    w_ent = -K.log(w_sum)[:, None, None, :]

    y_pred = tf.clip_by_value(y_pred, _epsilon, 1. - _epsilon)
    y_pred = -K.log(y_pred)
    
    return K.sum(y_pred * y_true * w_ent, axis=-1)


def custom_mape(y_true, y_pred):
    loss = K.mean(K.abs(y_true - y_pred) / K.clip(K.abs(y_true), K.epsilon(), None))
    # print('custom_loss:', loss)
    return loss

    
def custom_acc(y_true, y_pred):
    score = K.sum(K.cast(K.less_equal(K.abs(y_true - y_pred), 0.0001 + K.abs(y_true * 0.05)), y_true.dtype) * K.cast(K.greater(y_true, 0.001), y_true.dtype) * K.cast(K.greater(y_pred, 0.001), y_true.dtype)) / (1. + K.sum(K.cast(K.greater(y_true, 0.001), y_true.dtype)))
    return score

    
def custom_acc_0(y_true, y_pred):
    score = K.sum(K.cast(K.less_equal(y_true, 0.001), y_true.dtype) * K.cast(K.less_equal(y_pred, 0.001), y_true.dtype)) / (1. + K.sum(K.cast(K.less_equal(y_true, 0.001), y_true.dtype)))
    return score

    
def custom_acc_loss(y_true, y_pred):
    loss = 1. - K.sqrt(custom_acc(y_true, y_pred) * custom_acc_0(y_true, y_pred))
    return loss

    
def custom_dist(y_true, y_pred):
    loss = K.mean(K.square(K.pow(y_true, 2) - K.pow(y_pred, 2)))
    return loss

    
def false_positive(y_true, y_pred):
    # any 1+ class that should be 0
    t = K.reshape(y_true, (-1, 1))
    p = K.reshape(K.argmax(y_pred, axis=-1), (-1, 1))
    TN = K.sum(K.cast(K.equal(t, 0), y_true.dtype) * K.cast(K.equal(p, 0), y_true.dtype))
    FP = K.sum(K.cast(K.equal(t, 0), y_true.dtype) * K.cast(K.greater(p, 0), y_true.dtype))
    return 1. - TN / (TN + FP)


def cat_false_positive(y_true, y_pred):
    # any 1+ class that should be 0
    t = K.reshape(K.argmax(y_true, axis=-1), (-1, 1))
    p = K.reshape(K.argmax(y_pred, axis=-1), (-1, 1))
    TN = K.sum(K.cast(K.equal(t, 0), y_true.dtype) * K.cast(K.equal(p, 0), y_true.dtype))
    FP = K.sum(K.cast(K.equal(t, 0), y_true.dtype) * K.cast(K.greater(p, 0), y_true.dtype))
    return 1. - TN / (TN + FP)
    

classes = 9
def accuracy_entropy(y_true, y_pred):
    t = K.reshape(y_true, (-1, 1))
    p = K.reshape(K.argmax(y_pred, axis=-1), (-1, 1))
    
    cats = K.cast([K.mean(K.cast(K.equal(t, c), 'float32')) for c in range(classes)], 'float32')
    accs = K.cast([K.sum(K.cast(K.equal(t, c), 'float32') * K.cast(K.equal(p, c), 'float32')) / (K.sum(K.cast(K.equal(t, c), 'float32')) + K.epsilon()) for c in range(classes)], 'float32')

    entr = -K.log(cats)
    entr = K.minimum(entr, 7.)
    
    acc = K.sum(accs * entr) / K.sum(entr)

    return acc


def cat_accuracy_entropy(y_true, y_pred):
    t = K.reshape(K.argmax(y_true, axis=-1), (-1, 1))
    p = K.reshape(K.argmax(y_pred, axis=-1), (-1, 1))
    
    cats = K.cast([K.mean(K.cast(K.equal(t, c), 'float32')) for c in range(classes)], 'float32')
    accs = K.cast([K.sum(K.cast(K.equal(t, c), 'float32') * K.cast(K.equal(p, c), 'float32')) / (K.sum(K.cast(K.equal(t, c), 'float32')) + K.epsilon()) for c in range(classes)], 'float32')

    entr = -K.log(cats)
    entr = K.minimum(entr, 7.)
    
    acc = K.sum(accs * entr) / K.sum(entr)

    return acc
    

def cat_accuracy_entropy_score(y_true, y_pred):
    fpr = cat_false_positive(y_true, y_pred)
    fnr = cat_false_negative(y_true, y_pred)
    exp_fpr = K.exp(-fpr)
    exp_fnr = K.exp(-fnr)
    exp_fpr = tf.where(K.greater(exp_fpr, 0.6), exp_fpr, 0.01)
    exp_fnr = tf.where(K.greater(exp_fnr, 0.6), exp_fpr, 0.01)
    return cat_accuracy_entropy(y_true, y_pred) * exp_fpr * exp_fnr
      
    
def acc_entr_loss(y_true, y_pred):
    return 1. - accuracy_entropy(y_true, y_pred)


def false_negative(y_true, y_pred):
    t = K.reshape(y_true, (-1, 1))
    p = K.reshape(K.argmax(y_pred, axis=-1), (-1, 1))
    TP = K.sum(K.cast([K.sum(K.cast(K.equal(t, c), 'float32') * K.cast(K.equal(p, c), 'float32')) for c in range(1, classes)], 'float32'))
    FN = K.sum(K.cast(K.greater(t, 0), y_true.dtype) * K.cast(K.equal(p, 0), y_true.dtype))
    return 1. - TP / (TP + FN)


def cat_false_negative(y_true, y_pred):
    t = K.reshape(K.argmax(y_true, axis=-1), (-1, 1))
    p = K.reshape(K.argmax(y_pred, axis=-1), (-1, 1))
    TP = K.sum(K.cast([K.sum(K.cast(K.equal(t, c), 'float32') * K.cast(K.equal(p, c), 'float32')) for c in range(1, classes)], 'float32'))
    FN = K.sum(K.cast(K.greater(t, 0), y_true.dtype) * K.cast(K.equal(p, 0), y_true.dtype))
    return 1. - TP / (TP + FN)


def dunno_loss(y_true, y_pred):
    FPR = false_positive(y_true, y_pred)
    FNR = false_negative(y_true, y_pred)
    SCC = SparseCategoricalCrossentropy()(y_true, y_pred)
    return FPR + FNR + SCC
    
def prop_false_negative(y_true, y_pred):
    # any 0 class that should be 1+
    t = K.reshape(y_true, (-1, 1))
    p = K.reshape(K.argmax(y_pred, axis=-1), (-1, 1))
    return K.sum(K.cast(K.greater(t, 0), y_true.dtype) * K.cast(K.equal(p, 0), y_true.dtype)) / K.sum(K.cast(K.greater(t, 0), y_true.dtype))