import numpy as np
import netCDF4 as nc
import tensorflow as tf

import src.ncHelper as ncHelper
import src.grid_tricks as grid_tricks

from scipy.interpolate import RectBivariateSpline
from sklearn.model_selection import (
    train_test_split,
    StratifiedKFold,
)


class Dataset:
    def __init__(self, _input_path, _output_path, _dates, **kwargs):
        self.input_path = _input_path
        self.output_path = _output_path
        self.dates = np.asanyarray(_dates)
        self._initial_idx = 10 # test size
        self._max_idx = self.dates.size
        self._idx = 0
        self._day_block_size = 10 # train size
        
        self.norms = kwargs.get('norms', {})
        self.l_norms = kwargs.get('l_norms', {})
        self._categories = kwargs.get('cat', [])
        self.__chunks_dim = kwargs.get('chunks_dim', 32)
        self.__timesteps = kwargs.get('timesteps', None)
        self.__diff = kwargs.get('diff', False)
        self.__stacking = kwargs.get('stacking', False)
        self._test_slice_head = kwargs.get('test_slice_head', True)
        
        self._input_slice_lats = None
        self._input_slice_lons = None
        self.general_lats = None
        self.general_lons = None
        self._range_lats = np.array([-90., 90.])
        self._range_lons = np.array([-180., 180.])
        
        self._input_var_order = []
        self._per_variable_norm = {}
        self._output_var_order = []
        self._per_label_norm = {}
        self.num_features = 0
        self.__num_features = 0
        self.num_labels = 0
        
        self._x_train = None
        self._y_train = None
        self._x_test = None
        self._y_test = None
        
        self.x_train_chunks = None
        self.y_train_chunks = None
        self.x_validation_chunks = None
        self.y_validation_chunks = None
        self.x_test_chunks = None
        self.y_test_chunks = None
        
        self._eval()
        
        
    def _eval(self, ):
        _date = self.dates[0]
        
        _nc_input = nc.Dataset(self.input_path.format(date=_date), 'r', keepweakref=True)
        _nc_output = nc.Dataset(self.output_path.format(date=_date), 'r', keepweakref=True)
        
        _input_lats, _input_lons = ncHelper.get_lats_lons_file(_nc_input, 1, 0)
        _output_lats, _output_lons = ncHelper.get_lats_lons_file(_nc_output, 1, 0)
        
        _input_res = max(np.diff(_input_lats).max(), np.diff(_input_lons).max())
        _output_res = max(np.diff(_output_lats).max(), np.diff(_output_lons).max())
        
        print('resolutions: ', _input_res, _output_res, np.isclose(_input_res, _output_res))
        
        # keep input asis, down or up sample the output if needed and possible
        if _input_res >= _output_res:
            self._range_lats[0] = max(_input_lats.min(), _output_lats.min())
            self._range_lats[1] = min(_input_lats.max(), _output_lats.max())
            self._range_lons[0] = max(_input_lons.min(), _output_lons.min())
            self._range_lons[1] = min(_input_lons.max(), _output_lons.max())
            
            self._input_slice_lats = slice(np.argmin(abs(_input_lats - self._range_lats[0])),
                                           np.argmin(abs(_input_lats - self._range_lats[1])))
            self._input_slice_lons = slice(np.argmin(abs(_input_lons - self._range_lons[0])),
                                           np.argmin(abs(_input_lons - self._range_lons[1])))
            self.general_lats = _input_lats[self._input_slice_lats]
            self.general_lons = _input_lons[self._input_slice_lons]
            
        else:
            raise "input resolution is greater than the output's"
        
        _vars = {k: 0 for k in _nc_input.variables.keys()}
        _dims = {k: 0 for k in _nc_input.dimensions.keys()}
        for k in _dims.keys(): _vars.pop(k) # remove the dimensions from variables
        self._input_var_order = _vars.keys()
        
        self._per_variable_norm = {k: 1. for k in _vars.keys()}
        
        _vars = {k: 0 for k in _nc_output.variables.keys()}
        _dims = {k: 0 for k in _nc_output.dimensions.keys()}
        for k in _dims.keys(): _vars.pop(k) # remove the dimensions from variables
        self._output_var_order = _vars.keys()
        
        self._per_label_norm = {k: 1. for k in _vars.keys()}
        
        for k in self._input_var_order: # count number of features // does not have num of labels
            _shape = _nc_input.variables[k].shape
            if len(_shape) == 3:
                self.num_features += 1
            if len(_shape) == 4:
                self.num_features += _shape[1]
        
        self.__num_features = self.num_features
        
        self._per_variable_norm.update(self.norms)
        self._per_label_norm.update(self.l_norms)    
                
        for k in self._output_var_order: # count number of features // does not have num of labels
            _shape = _nc_output.variables[k].shape
            if len(_shape) == 3:
                self.num_labels += 1
            if len(_shape) == 4:
                self.num_labels += _shape[1]
                
        # pre process test data
        print('preprocessing test data')
        print('a total of %d indices'%self._initial_idx)
        self._x_test, self._y_test = [], [] # for plots (?)
        idxs = []
        # self.x_test_chunks, self.y_test_chunks = [], []
        _test_slice = slice(None, self._initial_idx) if self._test_slice_head else slice(-self._initial_idx, None)
        for _date in self.dates[_test_slice]:
        # for _date in self.dates[-10:]:
            # load stuff
            _x, _y, idx = self.process_data(_date, aux_idx=True)

            self._x_test.append(_x)
            self._y_test.append(_y)
            idxs.append(idx)
            
            # make chunks
            # _x_test_chunks, _y_test_chunks = grid_tricks.get_chunks(_x, _y, target_dim=(self.__chunks_dim, self.__chunks_dim))
            # self.x_test_chunks.append(_x_test_chunks)
            # self.y_test_chunks.append(_y_test_chunks)
            
            # print(_x.shape, _y.shape)
            # print(_x_test_chunks.shape, _y_test_chunks.shape)
            
        self._x_test = np.concatenate(self._x_test)
        self._y_test = np.concatenate(self._y_test)
        # self.x_test_chunks = np.concatenate(self.x_test_chunks)
        # self.y_test_chunks = np.concatenate(self.y_test_chunks)
        
        self._y_test_numerical = self._y_test.copy()
        # self.y_test_chunks_numerical = self.y_test_chunks.copy()
        self._transform_categorical(attr='_y_test')
        # self._transform_categorical(attr='y_test_chunks')
        self.idx_y_test = np.concatenate(idxs, axis=None)
        
        
    def _transform_categorical(self, attr):
        _temp_attr = getattr(self, attr)
        _rules = [(True if left is None else getattr(self, attr) >= left) & (True if right is None else getattr(self, attr) < right) for left, right in self._categories]
        for idx, _rule in enumerate(_rules):
            _temp_attr[_rule] = int(idx)
        _temp_attr = tf.one_hot(_temp_attr[..., 0], len(self._categories)).numpy()
        print('_temp_attr shape', _temp_attr.shape)
        setattr(self, attr, _temp_attr)
    
    
    def process_data(self, _date, **kwargs):
        self.num_features = self.__num_features
        
        _nc_input = nc.Dataset(self.input_path.format(date=_date), 'r', keepweakref=True)
        _nc_output = nc.Dataset(self.output_path.format(date=_date), 'r', keepweakref=True)
        
        _input_lats, _input_lons = ncHelper.get_lats_lons_file(_nc_input, 1, 0)
        _output_lats, _output_lons = ncHelper.get_lats_lons_file(_nc_output, 1, 0)
        
        _time_size = _nc_input['time'].size
        _x = np.empty((_time_size, self.num_features, self.general_lats.size, self.general_lons.size))
        _y = np.empty((_time_size, self.num_labels, self.general_lats.size, self.general_lons.size))
        
        _aux_idx = np.arange(_y.shape[0])
        # concat over time & cut/reshape
        _curr_index = 0
        # save some spacing
        _i = slice(None, _time_size)
        _j = self._input_slice_lats
        _k = self._input_slice_lons
        _lats = self.general_lats.size
        _lons = self.general_lons.size
        
        self._var_header = {}
        for k in self._input_var_order:
            _var = _nc_input.variables[k]
            _shape = _var.shape
            self._var_header[k] = _curr_index
            if len(_shape) == 3:
                if k in ['apcpsfc', 'acpcpsfc']:
                    _var = np.r_[_var[:1], _var[1:] - _var[:-1]] # gambi
                _x[:, slice(_curr_index, _curr_index + 1), :, :] = _var[_i, _j, _k].reshape(_time_size, -1, _lats, _lons) / self._per_variable_norm[k]
                _curr_index += 1
            elif len(_shape) == 4:
                _x[:, slice(_curr_index, _curr_index + _shape[1]), :, :] = _var[_i, :, _j, _k].reshape(_time_size, -1, _lats, _lons) / self._per_variable_norm[k]
                _curr_index += _shape[1]
                
        _curr_index = 0
        for k in self._output_var_order:
            _var = _nc_output.variables[k]
            _rect_func = lambda grid: RectBivariateSpline(_nc_output['latitude'][:], _nc_output['longitude'][:], grid)
            _shape = _var.shape
            if len(_shape) == 3:
                _temp_y = _var[slice(None, _time_size), :, :]
                # _rect_func = lambda grid: RectBivariateSpline(_nc_output['latitude'][:], _nc_output['longitude'][:], grid)
                _y[:, slice(_curr_index, _curr_index + 1), :, :] = np.reshape([_rect_func(_t_y)(self.general_lats, self.general_lons) for _t_y in _temp_y], (_time_size, -1, _lats, _lons)) / self._per_label_norm[k]
                _curr_index += 1
            elif len(_shape) == 4:
                _temp_y = _var[slice(None, _time_size), :, :, :]
                _y[:, slice(_curr_index, _curr_index + _shape[1]), :, :] = np.reshape([_rect_func(_t_y)(self.general_lats, self.general_lons) for _t_levels_y in temp_y for _t_y in _t_levels_y], (_time_size, -1, _lats, _lons)) / self._per_label_norm[k]
                # _y[:, slice(_curr_index, _curr_index + _shape[1]), :, :] = _var[slice(None, _time_size), :, self._input_slice_lats, self._input_slice_lons].reshape(_time_size, -1, self.general_lats.size, self.general_lons.size)
                _curr_index += _shape[1]
                
        
        # transpose
        _x = _x.transpose((0, 2, 3, 1))
        _y = _y.transpose((0, 2, 3, 1))
        
        if self.__stacking:
            _x = np.asanyarray([np.concatenate(_x[slice(i - 1, i + 2)], axis=-1) for i in range(1, _x.shape[0] - 1)])
            _y = _y[1:-1]
            
            self.num_features = _x.shape[-1]
            _aux_idx = _aux_idx[1:-1]
        
        if self.__diff:
            _x_diff = np.diff(_x, axis=0)
            _x = np.concatenate((_x[1:], _x_diff), axis=-1)
            _y = _y[1:]
            
            self.num_features = _x.shape[-1]
            _aux_idx = _aux_idx[1:]
        
        if isinstance(self.__timesteps, int):
            _x = np.asanyarray([_x[i: i + self.__timesteps] for i in range(_x.shape[0] - self.__timesteps + 1)])
            _y = _y[self.__timesteps - 2: -1] # uses one future step, TODO
            
            _aux_idx = _aux_idx[self.__timesteps - 2: -1]
                
        return (_x, _y) if not kwargs.get('aux_idx', False) else (_x, _y, _aux_idx)
        
        
    def _prepare_curr_dataset(self, ):
        _date = self.dates[self._idx]
        _x, _y = [], []
        if (self._idx + self._day_block_size) <= self._max_idx:
            print('building %d days of data'%self._day_block_size)
            for i in range(self._day_block_size):
                _temp_x, _temp_y = self.process_data(self.dates[self._idx + i])
                _x.append(_temp_x)
                _y.append(_temp_y)
        else:
            return

        _x = np.concatenate(_x, axis=0)
        _y = np.concatenate(_y, axis=0)
        # reserve
        # self._x = _x
        # self._y = _y
        
        print('_x:',_x.shape)
        print('_y:',_y.shape)
        
        #### no stratify
        self.x_train_chunks, self.y_train_chunks = grid_tricks.get_chunks(_x, _y, target_dim=(self.__chunks_dim, self.__chunks_dim))
        #### stratify # TODO get single points and stratify train and validation data separately
        # self.x_train_chunks, self.y_train_chunks, self.x_validation_chunks, self.y_validation_chunks = grid_tricks.get_stratified_shuffle_chunks(_x, _y, target_dim=(self.__chunks_dim, self.__chunks_dim))
        # self.x_train_chunks, self.y_train_chunks = grid_tricks.get_stratified_chunks(_x, _y, target_dim=(self.__chunks_dim, self.__chunks_dim), condition_max_value=[self._categories[0][1], 0.6])
        
        # self.x_train_chunks = self.x_train_chunks[stratify_less_zeros]
        # self.y_train_chunks = self.y_train_chunks[stratify_less_zeros]
        
        # to categorical
        self.y_train_chunks_numerical = self.y_train_chunks.copy()
        self._transform_categorical(attr='y_train_chunks')
        
        '''
        ### STRATIFY POST CATEGORIZATION
        
        chunks_mean = self.y_train_chunks.max(axis=(1, 2))
        categories = [(chunks_mean >= cat) & (chunks_mean < cat + 1) for cat in range(len(self._categories))]
        bigger_cat = min(np.sum(categories, axis=1).max(), 500)
        
        print('category sizes:', np.sum(categories, axis=1))
        categories = [(chunks_mean >= cat) & (chunks_mean < cat + 1) for cat in range(len(self._categories)) if ((chunks_mean >= cat) & (chunks_mean < cat + 1)).sum() > 0]
        
        new_x_chunks = []
        new_y_chunks = []
        new_y_chunks_numerical = []
        for category in categories:
            indices = np.random.choice(np.where(category)[0], size=bigger_cat)
            new_x_chunks.append(self.x_train_chunks[indices])
            new_y_chunks.append(self.y_train_chunks[indices])
            new_y_chunks_numerical.append(self.y_train_chunks_numerical[indices])
        
        self.x_train_chunks = np.concatenate(new_x_chunks, axis=0)
        self.y_train_chunks = np.concatenate(new_y_chunks, axis=0)
        self.y_train_chunks_numerical = np.concatenate(new_y_chunks_numerical, axis=0)
        '''

        
    def iterdates(self, norms={}, l_norms={}):

        return iter(self)
    
    def __iter__(self, ):
        self._idx = self._initial_idx
        return self
        
    def __next__(self, ):
        if self._idx < self._max_idx:
            _date = self.dates[self._idx]
            self._prepare_curr_dataset()
            self._idx += self._day_block_size
            return _date, self
        else:
            raise StopIteration