import itertools
import numpy as np

def get_chunks(*args, target_dim=(10, 10)):
    out = []
    for arg in args:
        # arg (batch, lat, lon, feats) or (batch, timesteps, lat, lon, feats)
        ichunks = arg.shape[-3] // target_dim[0]
        jchunks = arg.shape[-2] // target_dim[1]
        islices = [slice(i * target_dim[0], (i + 1) * target_dim[0]) for i in range(ichunks)]
        jslices = [slice(j * target_dim[1], (j + 1) * target_dim[1]) for j in range(jchunks)]
        pre_alloc_arg = np.zeros((*arg.shape[:-3], ichunks * jchunks, *target_dim, arg.shape[-1]))
        for idx, (islice, jslice) in enumerate(itertools.product(islices, jslices)):
            pre_alloc_arg[..., idx, :, :, :] = arg[..., islice, jslice, :]
        if len(pre_alloc_arg.shape) == 6:
            pre_alloc_arg = pre_alloc_arg.transpose((0, 2, 1, 3, 4, 5))
            out.append(pre_alloc_arg.reshape(-1, arg.shape[1], *target_dim, arg.shape[-1]))
        elif len(pre_alloc_arg.shape) == 5:
            out.append(pre_alloc_arg.reshape(-1, *target_dim, arg.shape[-1]))
        else:
            raise 'invalid target shape'
    return out
    
    
def get_stratified_chunks(x, y, target_dim=(10, 10), condition_max_value=[1., 0.5]):
    out = []
    
    ichunks = y.shape[-3] // target_dim[0]
    jchunks = y.shape[-2] // target_dim[1]
    islices = [slice(i * target_dim[0], (i + 1) * target_dim[0]) for i in range(ichunks)]
    jslices = [slice(j * target_dim[1], (j + 1) * target_dim[1]) for j in range(jchunks)]
    
    _x_arg = []
    _y_arg = []
    samples = 0
    for (islice, jslice) in itertools.product(islices, jslices):
        _y_chunk = y[:, islice, jslice, :]
        _condition = (_y_chunk < condition_max_value[0]).mean(axis=(1, 2, 3)) < condition_max_value[1]
        if _condition.sum() > 0:
            _x_arg.append(x[:, islice, jslice, :][_condition])
            _y_arg.append(_y_chunk[_condition])
            samples += _condition.sum()
    if samples < int(ichunks * jchunks * y.shape[0] * 0.3):
        print('less than %d samples, using condition %.2f'%(int(ichunks * jchunks * y.shape[0] * 0.3), condition_max_value[1] + 0.05))
        return get_stratified_chunks(x, y, target_dim, [condition_max_value[0], condition_max_value[1] + 0.05])
    # if samples > 10000:
    #     pick_samples = np.random.choice(np.arange(samples), size=10000, replace=False)
    #     _x_arg = np.concatenate(_x_arg, axis=0)[pick_samples]
    #     _y_arg = np.concatenate(_y_arg, axis=0)[pick_samples]
    #     return (_x_arg, _y_arg)
    return (np.concatenate(_x_arg, axis=0), np.concatenate(_y_arg, axis=0))
    

def get_stratified_shuffle_chunks(x, y, target_dim=(10, 10), categories=9):
    y_flat = np.argmax(y, axis=-1).reshape(-1, 1)
    x_flat = np.transpose(x, (0, 2, 3, 1, 4)).reshape(-1, x.shape[1], x.shape[4])
    # clear not enough data
    for cat in range(categories):
        mask_cat = y_flat == cat
        if mask_cat.sum() < 2:
            x_flat = x_flat[~mask_cat]
            y_flat = y_flat[~mask_cat]
        
    skf = StratifiedKFold(2)
    
    for train, valid in skf.split(x_flat, y_flat):
        x_train_singles, x_valid_singles = x_flat[train], x_flat[valid]
        y_train_singles, y_valid_singles = y_flat[train], y_flat[valid]
        break
    
    pass