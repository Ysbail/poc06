import numpy as np
import tensorflow as tf
import keras.regularizers as regularizers
import keras.backend as K
from keras.optimizers import SGD
from keras import initializers
from keras.models import (
    Sequential,
    Model
)
from keras.layers import (
    Add,
    Dot,
    ELU,
    ReLU,
    Conv2D,
    Conv3D,
    Input,
    Lambda,
    Average,
    Dropout,
    Maximum,
    Permute,
    Softmax,
    Subtract,
    LeakyReLU,
    Activation,
    ConvLSTM2D,
    Concatenate,
    concatenate,
    MaxPooling2D,
    UpSampling2D,
    SpatialDropout2D,
    AveragePooling2D,
    BatchNormalization,
)
from keras.activations import (
    sigmoid,
    tanh,
)
from keras.constraints import (
    NonNeg,
)
from src.customs import (
    custom_loss,
    custom_mape,
    custom_acc,
    custom_acc_0,
    custom_acc_loss,
    false_positive,
    false_negative,
    prop_false_negative,
    accuracy_entropy,
    acc_entr_loss,
    cat_false_positive,
    cat_false_negative,
    cat_accuracy_entropy,
    cat_accuracy_entropy_score,
)
from keras.metrics import (
    SparseCategoricalAccuracy,
    sparse_categorical_accuracy,
    categorical_accuracy,
)
from keras.losses import (
    BinaryCrossentropy,
    SparseCategoricalCrossentropy,
    CategoricalCrossentropy,
    categorical_crossentropy,
    
)
from keras.optimizers import (
    Nadam,
)
# sparse_categorical_accuracy = SparseCategoricalAccuracy()
'''
def model_perc_like(shape, loss='categorical_crossentropy'):
    _input = Input(shape=shape)
    
    out = medium_model(_input, name='perc')
    
    print(_input, out)
    
    model = Model(inputs=_input, outputs=out)
    # model.compile(loss=['sparse_categorical_crossentropy', custom_loss], optimizer='sgd', metrics=[false_positive, false_negative], loss_weights=[10., 1.]) # tf.keras.metrics.SparseTopKCategoricalAccuracy()
    model.compile(loss=loss, optimizer='adam', metrics=[cat_false_positive, cat_false_negative, cat_accuracy_entropy, categorical_accuracy, categorical_crossentropy])
    
    print(model.summary())
    
    return model
'''

def model_3d(shape, loss='categorical_crossentropy'):
    _input = Input(shape=shape)
    
    out = simple_3d_model(_input, name='3d')
    print(_input, out)
    
    model = Model(inputs=_input, outputs=out)
    model.compile(loss=loss, optimizer='adam', metrics=[cat_false_positive, cat_false_negative, cat_accuracy_entropy, cat_accuracy_entropy_score, categorical_accuracy, categorical_crossentropy])
    print(model.summary())
    
    return model
    

def model_stack(shape, n_weak=10):
    _input = Input(shape=shape)
    
    weak = []
    for i in range(n_weak):
        weak.append(mini_model(_input, name=str(i)))
    _meta_input = concatenate(weak)
    _meta = mini_model(_meta_input, name='meta')
    
    model = Model(inputs=_input, outputs=weak + [_meta])
    model.compile(loss='msle', optimizer='adam', metrics=['mse', 'mae', custom_mape, custom_acc, custom_acc_0, custom_acc_loss])
    
    print(model.summary())
    
    return model


def model_var_sequential(shape, ):
    _input = Input(shape=shape)
    out = []
    for i in range(shape[-1]):
        if len(out) > 1:
            out.append( mini_model( concatenate( [out[-1], Lambda(lambda x: x[:, :, :, i:i+1])(_input)] ) ) )
        else:
            out.append( mini_model( Lambda(lambda x: x[:, :, :, i:i+1])(_input) ) ) 
        
    model = Model(inputs=_input, outputs=out)
    model.compile(loss='logcosh', optimizer='sgd', metrics=[custom_acc, custom_acc_0, custom_acc_loss])
    
    print(model.summary())
    
    return model
        
def mini_model(_in, name=''):
    a   = Conv2D(filters=64, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='relu')(_in)
    b   = Conv2D(filters=32, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='relu')(a)
    c   = Conv2D(filters=16, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='relu')(b)
    d_1 = Conv2D(filters= 1, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='tanh')(c)
    d_2 = ReLU(name=name)(d_1)
    return d_2 


def weak_model(_in, name=''):
    
    a_1 = Conv2D(filters=16, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='tanh')(_in)
    # a_3 = Conv2D(filters=16, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='tanh')(_in)
    # a_5 = Conv2D(filters=16, kernel_size=(5, 5), strides=(1, 1), padding='same', activation='tanh')(_in)
    # a_c = concatenate([a_1, a_3, a_5])

    
    b_1 = Conv2D(filters=8, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='sigmoid')(a_1)
    # b_3 = Conv2D(filters=8, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='sigmoid')(a_c)
    # b_5 = Conv2D(filters=8, kernel_size=(5, 5), strides=(1, 1), padding='same', activation='sigmoid')(a_c)
    # b_c = concatenate([b_1, b_3, b_5])
    
    c_1 = Conv2D(filters=4, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='tanh')(b_1)
    # c_3 = Conv2D(filters=4, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='tanh')(b_c)
    # c_5 = Conv2D(filters=4, kernel_size=(5, 5), strides=(1, 1), padding='same', activation='tanh')(b_c)
    # c_c = concatenate([c_1, c_3, c_5])

    f_1 = Conv2D(filters=  1, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='sigmoid')(c_1)
    
    return f_1
    
def medium_model(_in, name=''):
    
    '''
    b_1 = Conv2D(filters=128, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='tanh')(a_c)
    b_3 = Conv2D(filters=128, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='tanh')(a_c)
    b_5 = Conv2D(filters=128, kernel_size=(5, 5), strides=(1, 1), padding='same', activation='tanh')(a_c)
    b_c = concatenate([b_1, b_3, b_5])
    # b_c_n = BatchNormalization(axis=3)(b_c)
    
    c_1 = Conv2D(filters=64, kernel_size=(1, 1), strides=(1, 1), padding='same', activation='tanh')(b_c)
    c_3 = Conv2D(filters=64, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='tanh')(b_c)
    c_5 = Conv2D(filters=64, kernel_size=(5, 5), strides=(1, 1), padding='same', activation='tanh')(b_c)
    c_c = concatenate([c_1, c_3, c_5])
    # c_c_n = BatchNormalization(axis=3)(c_c)
    '''
    
    kernel_init = 'random_uniform' # initializers.glorot_uniform()
    bias_init = initializers.Constant(value=0.2)
    
    # a_neurons = (64, 96, 128, 16, 32, 32)
    # b_neurons = (128, 128, 192, 32, 96, 64)
    # c_neurons = (192, 96, 208, 16, 48, 64)
    
    a_neurons = (np.array([64, 96, 128, 16, 32, 32]) * 1.5).astype(int)
    b_neurons = (np.array([128, 128, 192, 32, 96, 64]) * 1.5).astype(int)
    c_neurons = (np.array([192, 96, 208, 16, 48, 64]) * 1.5).astype(int)

    # a = inception_module(_in, *a_neurons, kernel_init, bias_init)
    a = inception_module_mod(_in, *a_neurons[:-1], kernel_init, bias_init)
    a = BatchNormalization()(a)
    # b = inception_module(a, *b_neurons, kernel_init, bias_init)
    b = inception_module_mod(a, *b_neurons[:-1], kernel_init, bias_init)
    b = BatchNormalization()(b)
    # c = inception_module(b, *c_neurons, kernel_init, bias_init)
    c = inception_module_mod(b, *c_neurons[:-1], kernel_init, bias_init)
    c = BatchNormalization()(c)

    f_3 = Conv2D(filters=1024, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='sigmoid', name='sub_categories_0')(c)
    # f_3 = LeakyReLU(0.3)(f_3)
    # f_3 = BatchNormalization()(f_3)
    # f_3 = MaxPooling2D((3, 3), strides=(1, 1), padding='same')(f_3)
    
    # f_3 = Conv2D(filters=128, kernel_size=(1, 1), strides=(1, 1),
    #              padding='same', activation='relu', name='sub_categories_1')(f_3)
    # f_3 = LeakyReLU(0.1)(f_3)
    # f_3 = BatchNormalization()(f_3)
    # f_3 = MaxPooling2D((3, 3), strides=(1, 1), padding='same')(f_3)
    
    # f_3 = Conv2D(filters=32, kernel_size=(1, 1), strides=(1, 1),
    #              padding='same', activation='relu', name='sub_categories_2')(f_3)
    # f_3 = LeakyReLU(0.1)(f_3)
    # f_3 = BatchNormalization()(f_3)
    # f_3 = MaxPooling2D((3, 3), strides=(1, 1), padding='same')(f_3)
    
    f_4 = Conv2D(filters=9, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='softmax', name='categorical')(f_3)

    return f_4


def medium_3d_model(_in, name=''):
    
    kernel_init = 'glorot_uniform' # initializers.glorot_uniform()
    bias_init = initializers.Constant(value=0.2)
    
    # a_neurons = (64, 96, 128, 16, 32, 32)
    # b_neurons = (128, 128, 192, 32, 96, 64)
    # c_neurons = (192, 96, 208, 16, 48, 64)
    
    a_neurons = (np.array([64, 96, 128, 16, 32, 32]) * 1.5).astype(int)
    b_neurons = (np.array([128, 128, 192, 32, 96, 64]) * 1.5).astype(int)
    c_neurons = (np.array([192, 96, 208, 16, 48, 64]) * 1.5).astype(int)
    
    _in_reduce = _5d_to_4d(_in)
    _in_reduce_norm = BatchNormalization()(_in_reduce)
    
    # a = inception_module(_in, *a_neurons, kernel_init, bias_init)
    a = inception_module(concatenate([_in_reduce, _in_reduce_norm]), *a_neurons, kernel_init, bias_init)
    a = BatchNormalization()(a)
    # b = inception_module(a, *b_neurons, kernel_init, bias_init)
    b = inception_module(a, *b_neurons, kernel_init, bias_init)
    b = BatchNormalization()(b)
    # c = inception_module(b, *c_neurons, kernel_init, bias_init)
    c = inception_module(b, *c_neurons, kernel_init, bias_init)
    c = BatchNormalization()(c)

    f_3 = Conv2D(filters=1024, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='relu', name='sub_categories_0')(concatenate([_in_reduce, c]))
    
    f_4 = Conv2D(filters=9, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='softmax', name='categorical')(f_3)

    return f_4
    

def simple_3d_model(_in, name=''):
    
    kernel_init = 'glorot_uniform' # initializers.glorot_uniform()
    bias_init = initializers.Constant(value=-0.2)
    
    # a_neurons = (64, 96, 128, 16, 32, 32)
    # b_neurons = (128, 128, 192, 32, 96, 64)
    # c_neurons = (192, 96, 208, 16, 48, 64)
    
    # a_neurons = (np.array([64, 96, 128, 16, 32, 32]) * 1.5).astype(int)
    # b_neurons = (np.array([128, 128, 192, 32, 96, 64]) * 1.5).astype(int)
    # c_neurons = (np.array([192, 96, 208, 16, 48, 64]) * 1.5).astype(int)
    
    _in_reduce = _5d_to_4d(_in)
    
    # a = inception_module(_in, *a_neurons, kernel_init, bias_init)
    
    a = Conv2D(1024, (1, 1), padding='same', activation='relu', kernel_initializer=kernel_init, bias_initializer=bias_init)(_in_reduce)
    # b = Conv2D(1024, (1, 1), padding='same', activation='relu', kernel_initializer=kernel_init, bias_initializer=bias_init)(a)
    # c = Conv2D(1024, (1, 1), padding='same', activation='relu', kernel_initializer=kernel_init, bias_initializer=bias_init)(b)

    # f_3 = Conv2D(filters=1024, kernel_size=(1, 1), strides=(1, 1),
    #              padding='same', activation='relu', name='sub_categories_0')(Add()([BatchNormalization()(_in_reduce), c]))
    
    # f_4 = CategoryEncoding(9, 'one_hot')(f_3)
    f_4 = Conv2D(filters=9, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='softmax', name='categorical')(a)

    return f_4

    
def inception_module(x,
                     filters_1x1,
                     filters_3x3_reduce,
                     filters_3x3,
                     filters_5x5_reduce,
                     filters_5x5,
                     filters_pool_proj,
                     kernel_init,
                     bias_init,
                     name=None):
    
    conv_1x1 = Conv2D(filters_1x1, (1, 1), padding='same', activation='relu', kernel_initializer=kernel_init, bias_initializer=bias_init)(x)
    
    conv_3x3 = Conv2D(filters_3x3_reduce, (1, 1), padding='same', activation='relu', kernel_initializer=kernel_init, bias_initializer=bias_init)(x)
    conv_3x3 = Conv2D(filters_3x3, (3, 3), padding='same', activation='relu', kernel_initializer=kernel_init, bias_initializer=bias_init)(conv_3x3)

    conv_5x5 = Conv2D(filters_5x5_reduce, (1, 1), padding='same', activation='relu', kernel_initializer=kernel_init, bias_initializer=bias_init)(x)
    conv_5x5 = Conv2D(filters_5x5, (5, 5), padding='same', activation='relu', kernel_initializer=kernel_init, bias_initializer=bias_init)(conv_5x5)

    pool_proj = MaxPooling2D((3, 3), strides=(1, 1), padding='same')(x)
    pool_proj = Conv2D(filters_pool_proj, (1, 1), padding='same', activation='relu', kernel_initializer=kernel_init, bias_initializer=bias_init)(pool_proj)

    output = concatenate([conv_1x1, conv_3x3, conv_5x5, pool_proj], axis=3, name=name)
    
    return output


def inception_module_mod(x,
                     filters_1x1,
                     filters_3x3_reduce,
                     filters_3x3,
                     filters_5x5_reduce,
                     filters_5x5,
                     # filters_pool_proj,
                     kernel_init,
                     bias_init,
                     name=None):
    
    conv_1x1 = Conv2D(filters_1x1, (1, 1), padding='same', activation='sigmoid', kernel_initializer=kernel_init, bias_initializer=bias_init)(x)
    
    conv_3x3 = Conv2D(filters_3x3_reduce, (1, 1), padding='same', activation='sigmoid', kernel_initializer=kernel_init, bias_initializer=bias_init)(x)
    conv_3x3 = Conv2D(filters_3x3, (3, 3), padding='same', activation='sigmoid', kernel_initializer=kernel_init, bias_initializer=bias_init)(conv_3x3)

    conv_5x5 = Conv2D(filters_5x5_reduce, (1, 1), padding='same', activation='sigmoid', kernel_initializer=kernel_init, bias_initializer=bias_init)(x)
    conv_5x5 = Conv2D(filters_5x5, (5, 5), padding='same', activation='sigmoid', kernel_initializer=kernel_init, bias_initializer=bias_init)(conv_5x5)

    # pool_proj = MaxPooling2D((3, 3), strides=(1, 1), padding='same')(x)
    # pool_proj = Conv2D(filters_pool_proj, (1, 1), padding='same', activation='relu', kernel_initializer=kernel_init, bias_initializer=bias_init)(pool_proj)

    output = concatenate([conv_1x1, conv_3x3, conv_5x5], axis=3, name=name)
    
    return output
    
def concat_reduce(x): # 5d to 4d
    shape = tf.shape(x)
    x_out = tf.reshape(x, [shape[0], shape[1], shape[2], shape[3] * shape[4]])
    return x_out
    
        
def _5d_to_4d(x):
    '''
    temporal feature extractor, needs at least 3 timesteps to work
    tries to extrapolate first and second derivatives in a ML way
    
    '''
    '''
    raw = Conv3D(filters=x.shape[-1], kernel_size=1, strides=1, padding='valid', activation='relu')(x)
    dt = Conv3D(filters=x.shape[-1], kernel_size=(2, 1, 1), strides=1, padding='valid', activation='relu')(x)
    d2t_from_raw = Conv3D(filters=x.shape[-1], kernel_size=(3, 1, 1), strides=1, padding='valid', activation='relu')(raw)
    d2t_from_dt = Conv3D(filters=x.shape[-1], kernel_size=(2, 1, 1), strides=1, padding='valid', activation='relu')(dt)

    concat = Concatenate(axis=1)([x, raw, dt, d2t_from_raw, d2t_from_dt])
    '''
    get_idx = lambda idx: Lambda(lambda x: x[:, idx: idx + 1, :, :, :], output_shape=(1, None, None, 145))
    
    a = get_idx(0)(x)
    b = get_idx(1)(x)
    c = get_idx(2)(x)
    
    dt_0 = Subtract()([b, a])
    dt_1 = Subtract()([c, b])
    d2t  = Subtract()([c, a])
    concat = Concatenate(axis=1)([b, dt_0, dt_1, d2t])
    perm = Permute((2, 3, 1, 4))(concat)
    output = Lambda(concat_reduce, output_shape=(None, None, perm.shape[-2] * perm.shape[-1]))(perm)
    
    return output
    
    
def get_vgg16(nx, ny, nz, loss=custom_loss, optimizer=None):
    model = Sequential()
    # Encoder
    # Block 1
    # model.add(BatchNormalization(axis=3, input_shape=(nx, ny, nz)))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu', name='block1_conv1', input_shape=(nx,ny,nz)))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu', name='block1_conv2'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool'))

    # Block 2
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu', name='block2_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu', name='block2_conv2'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool'))

    # Block 3
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu', name='block3_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu', name='block3_conv2'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu', name='block3_conv3'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool'))

    # Block 4
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block4_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block4_conv2'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block4_conv3'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool'))


    # Block 5
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block5_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block5_conv2'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block5_conv3'))
    
    # model.add(MaxPooling2D((2, 2), strides=(2, 2), name='block5_pool'))
    # intermission
    # model.add(BatchNormalization(axis=3))
    # model.add(Conv2D(4096, (1, 1), padding='same', activation='relu', name='block_inter_conv1'))
    # model.add(BatchNormalization(axis=3))
    # model.add(Conv2D(4096, (1, 1), padding='same', activation='relu', name='block_inter_conv2'))
    # model.add(BatchNormalization(axis=3))
    # model.add(Conv2D(1000, (1, 1), padding='same', activation='relu', name='block_inter_conv3'))
    
    # Decoder
    # Block 6
    model.add(UpSampling2D((2, 2), name='block6_upsampl'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block6_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block6_conv2'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block6_conv3'))
    
    # model.add(UpSampling2D((2, 2), name='block6.5_upsampl'))
    # model.add(BatchNormalization(axis=3))
    # model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block6.5_conv1'))
    # model.add(BatchNormalization(axis=3))
    # model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block6.5_conv2'))
    # model.add(BatchNormalization(axis=3))
    # model.add(Conv2D(512, (3, 3), padding='same', activation='relu', name='block6.5_conv3'))

    # Block 7
    model.add(UpSampling2D((2, 2), name='block7_upsampl'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu', name='block7_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu', name='block7_conv2'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(256, (3, 3), padding='same', activation='relu', name='block7_conv3'))

    # Block 8
    model.add(UpSampling2D((2, 2), name='block8_upsampl'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu', name='block8_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(128, (3, 3), padding='same', activation='relu', name='block8_conv2'))

    # Block 9
    model.add(UpSampling2D((2, 2), name='block9_upsampl'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu', name='block9_conv1'))
    model.add(BatchNormalization(axis=3))
    model.add(Conv2D(64, (3, 3), padding='same', activation='relu', name='block9_conv2'))

    # Output
    # model.add(BatchNormalization(axis=3))
    model.add(Conv2D(1, (1, 1), padding='same', activation='sigmoid', name='block10_conv1'))

    if optimizer is None:
        sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
        optimizer = sgd
    model.compile(loss=loss, optimizer=optimizer, metrics=[custom_mape, custom_acc, custom_acc_0, custom_acc_loss])
    
    print(model.summary())

    return model
    
    
def get_residual_vgg16(nx,ny,nz):
    # Encoder
    # Block 1
    _input = Input(shape=(nx, ny, nz))
    bn_0_0 = BatchNormalization(axis=3)(_input)
    conv2d_0_0 = Conv2D(64, (3, 3), padding='same', activation='linear', name='block1_conv1')(bn_0_0)
    relu_0_0 = ReLU(max_value=6)(conv2d_0_0)
    bn_0_1 = BatchNormalization(axis=3)(relu_0_0)
    conv2d_0_1 = Conv2D(64, (3, 3), padding='same', activation='linear', name='block1_conv2')(bn_0_1)
    resi_0 = Add()([conv2d_0_1, bn_0_1])
    relu_0_1 = ReLU(max_value=6)(resi_0)
    mp_0 = MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool')(relu_0_1)
    
    # Block 2
    bn_1_0 = BatchNormalization(axis=3)(mp_0)
    conv2d_1_0 = Conv2D(128, (3, 3), padding='same', activation='linear', name='block2_conv1')(bn_1_0)
    relu_1_0 = ReLU(max_value=6)(conv2d_1_0)
    bn_1_1 = BatchNormalization(axis=3)(relu_1_0)
    conv2d_1_1 = Conv2D(128, (3, 3), padding='same', activation='linear', name='block2_conv2')(bn_1_1)
    resi_1 = Add()([conv2d_1_1, bn_1_1])
    relu_1_1 = ReLU(max_value=6)(resi_1)
    mp_1 = MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool')(relu_1_1)

    # Block 3
    bn_2_0 = BatchNormalization(axis=3)(mp_1)
    conv2d_2_0 = Conv2D(256, (3, 3), padding='same', activation='linear', name='block3_conv1')(bn_2_0)
    relu_2_0 = ReLU(max_value=6)(conv2d_2_0)
    bn_2_1 = BatchNormalization(axis=3)(relu_2_0)
    conv2d_2_1 = Conv2D(256, (3, 3), padding='same', activation='linear', name='block3_conv2')(bn_2_1)
    resi_2 = Add()([conv2d_2_1, bn_2_1])
    relu_2_1 = ReLU(max_value=6)(resi_2)
    mp_2 = MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool')(relu_2_1)

    # Block 4
    bn_3_0 = BatchNormalization(axis=3)(mp_2)
    conv2d_3_0 = Conv2D(512, (3, 3), padding='same', activation='linear', name='block4_conv1')(bn_3_0)
    relu_3_0 = ReLU(max_value=6)(conv2d_3_0)
    bn_3_1 = BatchNormalization(axis=3)(relu_3_0)
    conv2d_3_1 = Conv2D(512, (3, 3), padding='same', activation='linear', name='block4_conv2')(bn_3_1)
    resi_3 = Add()([conv2d_3_1, bn_3_1])
    relu_3_1 = ReLU(max_value=6)(resi_3)
    mp_3 = MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool')(relu_3_1)


    # Block 5
    bn_4_0 = BatchNormalization(axis=3)(mp_3)
    conv2d_4_0 = Conv2D(512, (3, 3), padding='same', activation='linear', name='block5_conv1')(bn_4_0)
    relu_4_0 = ReLU(max_value=6)(conv2d_4_0)
    bn_4_1 = BatchNormalization(axis=3)(relu_4_0)
    conv2d_4_1 = Conv2D(512, (3, 3), padding='same', activation='linear', name='block5_conv2')(bn_4_1)
    resi_4 = Add()([conv2d_4_1, bn_4_1])
    relu_4_1 = ReLU(max_value=6)(resi_4)

    # Decoder
    # Block 6
    up_5 = UpSampling2D((2, 2), name='block6_upsampl')(relu_4_1)
    bn_5_0 = BatchNormalization(axis=3)(up_5)
    conv2d_5_0 = Conv2D(512, (3, 3), padding='same', activation='linear', name='block6_conv1')(bn_5_0)
    relu_5_0 = ReLU(max_value=6)(conv2d_5_0)
    bn_5_1 = BatchNormalization(axis=3)(relu_5_0)
    conv2d_5_1 = Conv2D(512, (3, 3), padding='same', activation='linear', name='block6_conv2')(bn_5_1)
    resi_5 = Add()([conv2d_5_1, bn_5_1])
    relu_5_1 = ReLU(max_value=6)(resi_5)

    # Block 7
    up_6 = UpSampling2D((2, 2), name='block7_upsampl')(relu_5_1)
    bn_6_0 = BatchNormalization(axis=3)(up_6)
    conv2d_6_0 = Conv2D(256, (3, 3), padding='same', activation='linear', name='block7_conv1')(bn_6_0)
    relu_6_0 = ReLU(max_value=6)(conv2d_6_0)
    bn_6_1 = BatchNormalization(axis=3)(relu_6_0)
    conv2d_6_1 = Conv2D(256, (3, 3), padding='same', activation='linear', name='block7_conv2')(bn_6_1)
    resi_6 = Add()([conv2d_6_1, bn_6_1])
    relu_6_1 = ReLU(max_value=6)(resi_6)

    # Block 8
    up_7 = UpSampling2D((2, 2), name='block8_upsampl')(relu_6_1)
    bn_7_0 = BatchNormalization(axis=3)(up_7)
    conv2d_7_0 = Conv2D(128, (3, 3), padding='same', activation='linear', name='block8_conv1')(bn_7_0)
    relu_7_0 = ReLU(max_value=6)(conv2d_7_0)
    bn_7_1 = BatchNormalization(axis=3)(relu_7_0)
    conv2d_7_1 = Conv2D(128, (3, 3), padding='same', activation='linear', name='block8_conv2')(bn_7_1)
    resi_7 = Add()([conv2d_7_1, bn_7_1])
    relu_7_1 = ReLU(max_value=6)(resi_7)

    # Block 9
    up_8 = UpSampling2D((2, 2), name='block9_upsampl')(relu_7_1)
    bn_8_0 = BatchNormalization(axis=3)(up_8)
    conv2d_8_0 = Conv2D(64, (3, 3), padding='same', activation='linear', name='block9_conv1')(bn_8_0)
    relu_8_0 = ReLU(max_value=6)(conv2d_8_0)
    bn_8_1 = BatchNormalization(axis=3)(relu_8_0)
    conv2d_8_1 = Conv2D(64, (3, 3), padding='same', activation='linear', name='block9_conv2')(bn_8_1)
    resi_8_1 = Add()([conv2d_8_1, bn_8_1])
    relu_8_1 = ReLU(max_value=6)(resi_8_1)

    # Output
    bn_9 = BatchNormalization(axis=3)(relu_8_1)
    conv2d_9_0 = Conv2D(1, (1, 1), padding='same', activation='relu', name='block10_conv1')(bn_9)

    model = Model(inputs=_input, outputs=conv2d_9_0)
    
    sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True) 
    model.compile(loss='logcosh', optimizer=sgd, metrics=['mae', 'mse', custom_mape, custom_acc, custom_acc_0, custom_acc_loss])
    #model.compile(loss='mae', optimizer=Adam(lr=0.001), metrics=['mse'])
    print(model.summary())

    return model



'''    
def encode_block(_input, _filters, block_num):

    bn_0 = BatchNormalization(axis=3)(_input)
    conv2d_0 = Conv2D(filters=_filters, (3, 3), padding='same', activation='linear', name='block%d_conv1'%block_num)(bn_0)
    relu_0 = ReLU(max_value=6)(conv2d_0)
    bn_1 = BatchNormalization(axis=3)(relu_0)
    conv2d_1 = Conv2D(filters=_filters, (3, 3), padding='same', activation='linear', name='block%d_conv2'%block_num)(bn_1)
    resi = Add()([conv2d_1, bn_1])
    relu_1 = ReLU(max_value=6)(resi)
    mp = MaxPooling2D((2, 2), strides=(2, 2), name='block%d_pool'%block_num)(relu_1)
    return mp
    
    
def neutral_block():
    pass
def decode_block():
    pass
'''

'''
def maxpooling_mayhem_model(_in, name=''): # test later, promising
    
    kernel_init = 'random_normal' # initializers.glorot_uniform()
    bias_init = initializers.Constant(value=0.2)
    
    a = inception_module(_in,  64,  96, 128, 16, 32, 32, kernel_init, bias_init) # original
    # a = inception_module(_in,  128,  192, 256, 32, 64, 1, kernel_init, bias_init)
    a = BatchNormalization()(a) # original
    b = inception_module(a, 128, 128, 192, 32, 96, 64, kernel_init, bias_init) # original
    # b = inception_module(a, 256, 256, 384, 64, 192, 128, kernel_init, bias_init)
    b = BatchNormalization()(b) # original
    c = inception_module(b, 192,  96, 208, 16, 48, 64, kernel_init, bias_init) # original
    # c = inception_module(b, 384,  192, 416, 32, 96, 128, kernel_init, bias_init)
    c = BatchNormalization()(c) # original
    

    f_3 = Conv2D(filters=1024, kernel_size=(3, 3), strides=(1, 1),
                 padding='same', activation='relu')(c)
    f_3 = MaxPooling2D((3, 3), strides=(1, 1), padding='same')(f_3)
    f_3 = Conv2D(filters=9 * 4, kernel_size=(3, 3), strides=(1, 1),
                 padding='same', activation='relu')(f_3)
    f_3 = MaxPooling2D((3, 3), strides=(1, 1), padding='same')(f_3)
    f_3 = Conv2D(filters=9 * 2, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='relu')(f_3)
    f_3 = MaxPooling2D((3, 3), strides=(1, 1), padding='same')(f_3)
    f_3 = Conv2D(filters=9, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='softmax')(f_3)
    
    return f_3    

def aira2_c4_v1(_in, name=''):
    # .dot([0, 15, 30, 100])
    kernel_init = 'random_normal' # initializers.glorot_uniform()
    bias_init = initializers.Constant(value=0.2)
    
    a = inception_module(_in,  64,  96, 128, 16, 32, 32, kernel_init, bias_init)
    a = BatchNormalization()(a)
    b = inception_module(a, 128, 128, 192, 32, 96, 64, kernel_init, bias_init)
    b = BatchNormalization()(b)
    c = inception_module(b, 192,  96, 208, 16, 48, 64, kernel_init, bias_init)
    c = BatchNormalization()(c)
 
    f_3 = Conv2D(filters=1024, kernel_size=(3, 3), strides=(1, 1),
                 padding='same', activation='relu')(c)
    f_3 = BatchNormalization()(f_3)
    f_3 = Conv2D(filters=4, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='softmax')(f_3)
    return f_3

def aira2_c9_v1(_in, name=''):
    # loss = 1. + loss_scc - K.exp(-fp) * K.exp(-fn)
    # optimizer = 'nadam'
    kernel_init = 'random_normal'
    bias_init = initializers.Constant(value=0.2)
    
    a = inception_module(_in,  64,  96, 128, 16, 32, 32, kernel_init, bias_init)
    a = BatchNormalization()(a)
    b = inception_module(a, 128, 128, 192, 32, 96, 64, kernel_init, bias_init)
    b = BatchNormalization()(b)
    c = inception_module(b, 192,  96, 208, 16, 48, 64, kernel_init, bias_init)
    c = BatchNormalization()(c)
    f_3 = Conv2D(filters=256, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='linear', name='sub_categories_0')(c)
    f_3 = LeakyReLU(0.1)(f_3)
    f_4 = Conv2D(filters=9, kernel_size=(1, 1), strides=(1, 1),
                 padding='same', activation='softmax', name='categorical')(f_3)
    return f_4
'''