import src.models as models
import src.model_tricks as model_tricks
from keras.models import load_model
from src.dataset import Dataset
import numpy as np
from src.customs import (
    custom_loss,
    custom_mape,
    custom_acc,
    custom_acc_0,
    custom_acc_loss,
    false_positive,
    false_negative,
    prop_false_negative,
    dunno_loss,
    accuracy_entropy,
    cat_false_positive,
    cat_false_negative,
    cat_accuracy_entropy,
    cat_accuracy_entropy_score,
)
from keras.callbacks import (
    ModelCheckpoint,
    EarlyStopping,
    ReduceLROnPlateau
)
from keras.metrics import (
    SparseCategoricalAccuracy,
    sparse_categorical_accuracy,
)

# sparse_categorical_accuracy = SparseCategoricalAccuracy()

import pandas as pd
import datetime as dt
import tensorflow as tf
import tensorflow.keras.backend as K

import glob

K.clear_session()

dtNow = dt.datetime.utcnow()

input_path = '/storagefinep/wrf/dataset/{date:%Y%m%d}_wrf_dataset.nc'
output_path = '/storagefinep/merge/dataset/{date:%Y%m%d}_merge_dataset.nc'

epochs = 5000
batch_size = 32
best_model_pattern = "./models/best_{}.h5"
monitor_metric = 'val_cat_accuracy_entropy_score' # 'val_cat_accuracy_entropy'
verb = 2
earlystop_mode = 'max'
earlystop_monitor = 'val_cat_accuracy_entropy_score' # 'val_cat_accuracy_entropy'
earlystop_patience = 50

first = True
model = None

date_ini = '2021-01-01'
date_fin = '2021-05-01'

dates = pd.date_range(date_ini, date_fin, freq='D').to_pydatetime()
fmodel = f'./models/{dtNow:%Y%m%dT%H%M}_aira2_poc06.h5'

# norms
_norms = {'hgtprs': 5800., 
          'tmpprs': 204., 
          'rhprs': 100., 
          'dptprs': 298., 
          'wind_speed': 30., 
          'wind_dir': 360., 
          'tmpsfc': 305., 
          'tmp2m': 310., 
          'rh2m': 100., 
          'wind_direction': 360., 
          'wind_magnitude': 30., 
          'acpcpsfc': 150., 
          'cpratsfc': 150.,
          'apcpsfc': 100.,
          'crainsfc': 100.,
          'pratesfc': 100., 
          'capesfc': 50.,
          }
          
_l_norms = {'precipitation': 100.,
            }
            
# cats
_cat = [[None, 0.005],
        [0.005, 0.01],
        [0.01,  0.03],
        [0.03,  0.05],
        [0.05,   0.1],
        [0.1,   0.15],
        [0.15,  0.25],
        [0.25,   0.3],
        [0.3,   None]]
        
# prepare datasets iter
datasets = Dataset(input_path,
                   output_path,
                   dates,
                   # stacking=True,
                   timesteps=3,
                   cat=_cat,
                   norms=_norms,
                   l_norms=_l_norms)

print(datasets._input_var_order)
print(datasets.num_features)

print(datasets._output_var_order)
print(datasets.num_labels)

best_model = f'./models/bkp_{dtNow:%Y%m%dT%H%M}_aira2_poc06.h5' # best_model_pattern.format(date.strftime('%Y%m%d'))
checkpoint = ModelCheckpoint(best_model, monitor=monitor_metric, verbose=verb, save_best_only=True, mode=earlystop_mode)
earlystop = EarlyStopping(monitor=earlystop_monitor, patience=earlystop_patience, mode=earlystop_mode, verbose=verb, restore_best_weights=True)
callbacks_list = [checkpoint, earlystop]

for date, dataset in datasets.iterdates():
    # dataset.y_train_chunks = dataset.y_train_chunks
    # dataset.y_test_chunks = dataset.y_test_chunks
    
    x_train = dataset.x_train_chunks # dataset._x_train_chunks
    y_train = dataset.y_train_chunks # [dataset.y_train_chunks, dataset.y_train_chunks_numerical]
    
    x_test = dataset._x_test # dataset._x_test_chunks
    y_test = dataset._y_test # [dataset.y_test_chunks, dataset.y_test_chunks_numerical]
    
    # y_train = dataset._y_train
    # y_test = dataset._y_test
    
    if first:
        first = False
        K.clear_session()
        model = models.model_3d((3, None, None, dataset.num_features)) # , custom_loss)
    else:
        model.load_weights(best_model)
    
    if glob.glob(fmodel) == []:
        print("[I] Saving first model")
        model.save(fmodel)
        
        hist_old = []
        for i in range(y_test.shape[0]):
            hist_old.append(model.evaluate(x_test[i: i + 1], y_test[i: i + 1], verbose=0))
        hist_old = np.mean(hist_old, axis=0)

    print('before fit: ', hist_old)
    
    model.fit(x_train, y_train, epochs=epochs, verbose=verb, validation_split=0.2, callbacks=callbacks_list)

    hist_new = []
    for i in range(y_test.shape[0]):
        hist_new.append(model.evaluate(x_test[i: i + 1], y_test[i: i + 1], verbose=0))
    hist_new = np.mean(hist_new, axis=0)

    print('before fit: ', hist_old)
    print('after fit: ', hist_new)
    
    hist_old = hist_new
    
    print("[I] Saving model")
    model.save(fmodel)